<?php

use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $t1 = \App\Models\Teacher::find(1);
        $t2 = \App\Models\Teacher::find(2);
        $t3 = \App\Models\Teacher::find(3);

        $s1 = \App\Models\Subject::find(1);
        $s2 = \App\Models\Subject::find(2);
        $s3 = \App\Models\Subject::find(3);
        $s4 = \App\Models\Subject::find(4);
        $s5 = \App\Models\Subject::find(5);
        $s6 = \App\Models\Subject::find(6);
        $s7 = \App\Models\Subject::find(7);
        $s8 = \App\Models\Subject::find(8);
        $s9 = \App\Models\Subject::find(9);
        $s10 = \App\Models\Subject::find(10);
        $s11 = \App\Models\Subject::find(11);
        $s12 = \App\Models\Subject::find(12);

        $l00 = new \App\Models\Lesson();
        $l00->date = \Carbon\Carbon::today()->format('Y-m-d');
        $l00->start = '16:00:00';
        $l00->end = '18:00:00';
        $l00->max_students = 10;
        $l00->teacher()->associate($t2);
        $l00->subject()->associate($s4);
        $l00->save();

        $l0 = new \App\Models\Lesson();
        $l0->date = \Carbon\Carbon::today()->format('Y-m-d');
        $l0->start = '18:00:00';
        $l0->end = '20:00:00';
        $l0->max_students = 10;
        $l0->teacher()->associate($t3);
        $l0->subject()->associate($s10);
        $l0->save();
        
        $l1 = new \App\Models\Lesson();
        $l1->date = \Carbon\Carbon::tomorrow()->format('Y-m-d');
        $l1->start = '16:00:00';
        $l1->end = '18:00:00';
        $l1->max_students = 10;
        $l1->teacher()->associate($t1);
        $l1->subject()->associate($s1);
        $l1->save();

        $l2 = new \App\Models\Lesson();
        $l2->date = \Carbon\Carbon::tomorrow()->format('Y-m-d');
        $l2->start = '18:00:00';
        $l2->end = '20:00:00';
        $l2->max_students = 10;
        $l2->teacher()->associate($t1);
        $l2->subject()->associate($s2);
        $l2->save();

        $l3 = new \App\Models\Lesson();
        $l3->date = \Carbon\Carbon::today()->addDays(2)->format('Y-m-d');
        $l3->start = '16:00:00';
        $l3->end = '18:00:00';
        $l3->max_students = 10;
        $l3->teacher()->associate($t1);
        $l3->subject()->associate($s1);
        $l3->save();

        $l4 = new \App\Models\Lesson();
        $l4->date = \Carbon\Carbon::today()->addDays(2)->format('Y-m-d');
        $l4->start = '18:00:00';
        $l4->end = '20:00:00';
        $l4->max_students = 10;
        $l4->teacher()->associate($t1);
        $l4->subject()->associate($s3);
        $l4->save();

        $l5 = new \App\Models\Lesson();
        $l5->date = \Carbon\Carbon::today()->addDays(3)->format('Y-m-d');
        $l5->start = '16:00:00';
        $l5->end = '18:00:00';
        $l5->max_students = 10;
        $l5->teacher()->associate($t2);
        $l5->subject()->associate($s5);
        $l5->save();

        $l6 = new \App\Models\Lesson();
        $l6->date = \Carbon\Carbon::today()->addDays(3)->format('Y-m-d');
        $l6->start = '18:00:00';
        $l6->end = '20:00:00';
        $l6->max_students = 10;
        $l6->teacher()->associate($t2);
        $l6->subject()->associate($s6);
        $l6->save();

        $l7 = new \App\Models\Lesson();
        $l7->date = \Carbon\Carbon::today()->addDays(4)->format('Y-m-d');
        $l7->start = '16:00:00';
        $l7->end = '18:00:00';
        $l7->max_students = 10;
        $l7->teacher()->associate($t3);
        $l7->subject()->associate($s11);
        $l7->save();

        $l8 = new \App\Models\Lesson();
        $l8->date = \Carbon\Carbon::today()->addDays(4)->format('Y-m-d');
        $l8->start = '18:00:00';
        $l8->end = '20:00:00';
        $l8->max_students = 10;
        $l8->teacher()->associate($t3);
        $l8->subject()->associate($s12);
        $l8->save();

        $l9 = new \App\Models\Lesson();
        $l9->date = \Carbon\Carbon::today()->addDays(5)->format('Y-m-d');
        $l9->start = '09:00:00';
        $l9->end = '11:00:00';
        $l9->max_students = 10;
        $l9->teacher()->associate($t1);
        $l9->subject()->associate($s7);
        $l9->save();

        $l10 = new \App\Models\Lesson();
        $l10->date = \Carbon\Carbon::today()->addDays(5)->format('Y-m-d');
        $l10->start = '11:00:00';
        $l10->end = '13:00:00';
        $l10->max_students = 10;
        $l10->teacher()->associate($t1);
        $l10->subject()->associate($s8);
        $l10->save();
    }
}
