<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $s1 = new \App\Models\Subject();
        $s1->title = 'פיזיקה';
        $s1->level = 'א';
        $s1->save();
        
        $s2 = new \App\Models\Subject();
        $s2->title = 'פיזיקה';
        $s2->level = 'ב';
        $s2->save();
        
        $s3 = new \App\Models\Subject();
        $s3->title = 'פיזיקה';
        $s3->level = 'ג';
        $s3->save();
        
        $s4 = new \App\Models\Subject();
        $s4->title = 'מתמטיקה';
        $s4->level = 'א';
        $s4->save();
        
        $s5 = new \App\Models\Subject();
        $s5->title = 'מתמטיקה';
        $s5->level = 'ב';
        $s5->save();
        
        $s6 = new \App\Models\Subject();
        $s6->title = 'מתמטיקה';
        $s6->level = 'ג';
        $s6->save();
        
        $s7 = new \App\Models\Subject();
        $s7->title = 'כימיה';
        $s7->level = 'א';
        $s7->save();
        
        $s8 = new \App\Models\Subject();
        $s8->title = 'כימיה';
        $s8->level = 'ב';
        $s8->save();
        
        $s9 = new \App\Models\Subject();
        $s9->title = 'כימיה';
        $s9->level = 'ג';
        $s9->save();

        $s10 = new \App\Models\Subject();
        $s10->title = 'אנגלית';
        $s10->level = 'א';
        $s10->save();

        $s11 = new \App\Models\Subject();
        $s11->title = 'אנגלית';
        $s11->level = 'ב';
        $s11->save();

        $s12 = new \App\Models\Subject();
        $s12->title = 'אנגלית';
        $s12->level = 'ג';
        $s12->save();

        $t1 = \App\Models\Teacher::find(1);
        $t1->subjects()->attach($s1, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        $t1->subjects()->attach($s2, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        $t1->subjects()->attach($s3, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        $t1->subjects()->attach($s7, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        $t1->subjects()->attach($s8, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        $t1->subjects()->attach($s9, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);

        $t2 = \App\Models\Teacher::find(2);
        $t2->subjects()->attach($s4, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        $t2->subjects()->attach($s5, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        $t2->subjects()->attach($s6, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);

        $t3 = \App\Models\Teacher::find(3);
        $t3->subjects()->attach($s10, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        $t3->subjects()->attach($s11, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        $t3->subjects()->attach($s12, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);

    }
}
