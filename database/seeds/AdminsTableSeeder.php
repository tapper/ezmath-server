<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $a1 = new \App\Models\Admin();
        $a1->username = 'admin1';
        $a1->password = 'admin1';
        $a1->save();

        $a2 = new \App\Models\Admin();
        $a2->username = 'admin2';
        $a2->password = 'admin2';
        $a2->save();

        $b1 = \App\Models\Branch::find(1);
        $a1->branches()->attach($b1, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);

        $b2 = \App\Models\Branch::find(2);
        $b3 = \App\Models\Branch::find(3);
        $a2->branches()->attach($b2, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        $a2->branches()->attach($b3, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
    }
}
