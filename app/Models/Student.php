<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'city', 'grade', 'level', 'school_name', 'parent_name', 'parent_phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function cardbooks()
    {
        return $this->belongsToMany(Cardbook::class);
    }

    public function lessons()
    {
        return $this->belongsToMany(Lesson::class)->withPivot('visited');
    }
}
