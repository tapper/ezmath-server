<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function branches()
    {
        return $this->belongsToMany(Branch::class);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class);
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }
}
