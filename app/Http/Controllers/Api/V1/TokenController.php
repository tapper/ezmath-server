<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\CreateStudentToken;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TokenController extends Controller
{

    public function login(CreateStudentToken $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!Auth::guard('web')->validate($credentials))
            return responder()->error('login_again')->respond();

        $student = Student::whereEmail($request->get('email'))->first();
        $student->api_token = str_random(60);
        $student->push_id = $request->get('push_id');
        $student->save();

        return responder()->success($student, function ($student) {
            return ['id' => $student->id, 'api_token' => $student->api_token];
        })->respond();
    }
    
}
