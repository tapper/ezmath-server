<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Lesson;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LessonController extends Controller
{
    /**
     * Lessons for specified day, branch, subject.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->guard('api')->user();
        $lessons = Lesson::where('subject_id', $request->get('subject_id'))->where('date', '>=', $request->get('date'))->get();
        $lessons->load('students');
        foreach ($lessons as $lesson) {
            $lesson->status = $lesson->students->count() < $lesson->max_students ? 'available' : 'full';
            $enrolled = $lesson->students->filter(function ($student) use ($user) {
                return $student->id === $user->id;
            });
            $lesson->enrolled = $enrolled->count() > 0;
        }
        return responder()->success($lessons);
    }

    /**
     * Attach lesson to student.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lesson = Lesson::find($request->get('lesson_id'));
        $student = auth()->guard('api')->user();
        $lesson->students()->attach($student, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);

        return responder()->success();
    }

    /**
     * Detach lesson from student
     * 
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function detach(Request $request)
    {
        $lesson = Lesson::find($request->get('lesson_id'));
        $student = auth()->guard('api')->user();
        $lesson->students()->detach($student);

        return responder()->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Lesson $lesson)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function showLessons(Request $request)
    {
        $type = $request->get('type');
        $student = auth()->guard('api')->user();
        $lessons = $student->lessons;

        if ($type == 'forward'){
            $lessons = $lessons->filter(function ($lesson){
                $lesson->formatted_date = Carbon::createFromFormat('Y-m-d H:i:s', $lesson->date . ' ' . $lesson->end);
                return $lesson->formatted_date->gt(Carbon::now());
            })->values()->all();
        } else {
            $lessons = $lessons->filter(function ($lesson){
                $lesson->formatted_date = Carbon::createFromFormat('Y-m-d H:i:s', $lesson->date . ' ' . $lesson->end);
                return $lesson->formatted_date->lt(Carbon::now());
            })->values()->all();
        }

        return responder()->success($lessons);
    }
}
